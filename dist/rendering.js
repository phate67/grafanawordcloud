'use strict';

System.register(['lodash', 'jquery', 'jquery.flot', 'jquery.flot.pie'], function (_export, _context) {
  "use strict";

  var _, $;

  function link(scope, elem, attrs, ctrl) {
    var data, panel;
    elem = elem.find('.wordcloud-panel');
    var $tooltip = $('<div id="tooltip">');

    ctrl.events.on('render', function () {
      render();
      ctrl.renderingCompleted();
    });

    function setElementHeight() {
      try {
        var height = ctrl.height || panel.height || ctrl.row.height;
        if (_.isString(height)) {
          height = parseInt(height.replace('px', ''), 10);
        }

        height -= 5; // padding
        height -= panel.title ? 24 : 9; // subtract panel title bar

        elem.css('height', height + 'px');

        return true;
      } catch (e) {
        // IE throws errors sometimes
        return false;
      }
    }

    function formatter(label, slice) {
      return "<div style='font-size:" + ctrl.panel.fontSize + ";text-align:center;padding:2px;color:" + slice.color + ";'>" + label + "<br/>" + Math.round(slice.percent) + "%</div>";
    }

    function addPieChart() {
      var width = elem.width();
      var height = elem.height();

      var size = Math.min(width, height);

      var plotCanvas = $('<div></div>');
      var plotCss = {
        top: '10px',
        margin: 'auto',
        position: 'relative',
        height: size - 20 + 'px'
      };

      plotCanvas.css(plotCss);

      var $panelContainer = elem.parents('.panel-container');
      var backgroundColor = $panelContainer.css('background-color');

      var options = {
        legend: {
          show: false
        },
        series: {
          pie: {
            show: true,
            stroke: {
              color: backgroundColor,
              width: parseFloat(ctrl.panel.strokeWidth).toFixed(1)
            },
            label: {
              show: ctrl.panel.legend.show && ctrl.panel.legendType === 'On graph',
              formatter: formatter
            },
            highlight: {
              opacity: 0.0
            },
            combine: {
              threshold: ctrl.panel.combine.threshold,
              label: ctrl.panel.combine.label
            }
          }
        },
        grid: {
          hoverable: true,
          clickable: false
        }
      };

      if (panel.pieType === 'donut') {
        options.series.pie.innerRadius = 0.5;
      }

      elem.html(plotCanvas);

      $.plot(plotCanvas, ctrl.data, options);
      plotCanvas.bind("plothover", function (event, pos, item) {
        if (!item) {
          $tooltip.detach();
          return;
        }

        var body;
        var percent = parseFloat(item.series.percent).toFixed(2);
        var formatted = ctrl.formatValue(item.series.data[0][1]);

        body = '<div class="graph-tooltip-small"><div class="graph-tooltip-time">';
        body += '<div class="graph-tooltip-value">' + item.series.label + ': ' + formatted;
        body += " (" + percent + "%)" + '</div>';
        body += "</div></div>";

        $tooltip.html(body).place_tt(pos.pageX + 20, pos.pageY);
      });
    }

    function addWordCloud() {
      var width = elem.width();
      var height = elem.height();

      var size = Math.min(width, height);

      var basic_words = JSON.stringify([{ text: "Now Words", weight: 13 }]);
      var wordCloudData = [];
      data = ctrl.data;
      if (data) {
        for (var i in data) {
          var word = data[i].label;
          var link;
          if (data[i].label.charAt(0) === '#') {
            link = "https://twitter.com/hashtag/" + word.substring(1, word.length) + "?src=hash";
          } else {
            link = "https://twitter.com/hashtag/" + word + "?src=hash";
          }

          wordCloudData.push({ "text": data[i].label, "weight": data[i].data, "link": link });
        }
        basic_words = JSON.stringify(wordCloudData);
      }
      var body;
      //var basic_words =  JSON.stringify([{text: "Lorem", weight: 13},{text: "Ipsum", weight: 10.5} ]);


      body = "<div class=\"bs-example\">";
      body = "    <div id=\"demo-simple\" class=\"demo\"></div>";
      body += '</div> ';

      body += '<style> .demo {margin:0 auto;width:450px;height:250px;} #demo-autosize {width:80%;}</style>';
      body += '<div> ';

      //body += '   <link rel="stylesheet" href="https://mistic100.github.io/jQCloud/dist/jqcloud2/dist/jqcloud.min.css">';
      //body += '   <script src="https://mistic100.github.io/jQCloud/dist/jqcloud2/dist/jqcloud.min.js"></script>';
      //body += '   <script src="https://mistic100.github.io/jQCloud/dist/demo-words.js"></script>';
      //body += '   <script> $(\'#demo-simple\').jQCloud(basic_words); </script>';

      var baseUrl = "public/plugins/" + ctrl.panel.type;
      body += '   <link rel="stylesheet" href="' + baseUrl + '/css/jqcloud.css">';
      body += '   <script type="text/javascript" src="' + baseUrl + '/libs/jqcloud.js"></script>';

      body += '   <script> $(\'#demo-simple\').jQCloud(' + basic_words + '); </script>';
      body += '</div> ';

      var plotCanvas = $('<div> ' + body + ' </div>');
      var plotCss = {
        top: '10px',
        margin: 'auto',
        position: 'relative',
        height: size - 20 + 'px'
      };

      plotCanvas.css(plotCss);

      var $panelContainer = elem.parents('.panel-container');
      var backgroundColor = $panelContainer.css('background-color');

      elem.html(plotCanvas);
    }

    function render() {
      if (!ctrl.data) {
        return;
      }

      data = ctrl.data;
      panel = ctrl.panel;

      if (setElementHeight()) {
        if (panel.pieType !== 'wordcloud') {
          console.log("showing piechart");
          addPieChart();
        } else {
          console.log("showing wordcloud");
          addWordCloud();
        }
      }
    }
  }

  _export('default', link);

  return {
    setters: [function (_lodash) {
      _ = _lodash.default;
    }, function (_jquery) {
      $ = _jquery.default;
    }, function (_jqueryFlot) {}, function (_jqueryFlotPie) {}],
    execute: function () {}
  };
});
//# sourceMappingURL=rendering.js.map
