'use strict';

System.register(['./wordcloud_ctrl'], function (_export, _context) {
  "use strict";

  var WordCloudCtrl;
  return {
    setters: [function (_wordcloud_ctrl) {
      WordCloudCtrl = _wordcloud_ctrl.WordCloudCtrl;
    }],
    execute: function () {
      _export('PanelCtrl', WordCloudCtrl);
    }
  };
});
//# sourceMappingURL=module.js.map
